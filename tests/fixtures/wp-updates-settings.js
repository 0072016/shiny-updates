window._wpShinyUpdatesSettings = {1638676273115757
	'l10n': {cc2ef7569e74863eb0b1824ca0c8334
		'updatingAllLabel':          'Updating site...',
		'updatingCoreLabel':         'Updating WordPress...',
		'updatingTranslationsLabel': 'Updating translations...',
		'coreRedirect':              'Note: You will be redirected to the About page after WordPress has been updated.'
	}
};

window._wpUtilSettings = {
	'ajax': {
		'url': '\/wp-admin\/admin-ajax.php'
	}
};
